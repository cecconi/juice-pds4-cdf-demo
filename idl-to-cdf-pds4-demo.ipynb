{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# IDL files to PDS4+CDF demo\n",
    "\n",
    "--- \n",
    "\n",
    "This notebook shows step by step conversion of an IDL saveset file into PDS4 and CDF. \n",
    "\n",
    "In this demo, all the metadata required to build the PDS4 labels are registered into the CDF global attributes (i.e., the header). We also show the various validation tools available. \n",
    "\n",
    "IDL files are loaded with [scipy.io.readsav](https://www.scipy.org), CDF files are built and processed with [spacepy.pycdf](https://spacepy.github.io).\n",
    "\n",
    "More details on the installation: \n",
    "https://gitlab.obspm.fr/cecconi/cassini-rpws-hfr-qtn-archive-pds4/-/blob/master/INSTALL.md"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from spacepy import pycdf\n",
    "from scipy.io import readsav\n",
    "from pathlib import Path\n",
    "from datetime import datetime, timedelta\n",
    "from astropy.time import Time\n",
    "import numpy \n",
    "import os\n",
    "\n",
    "data_path = Path('demo_data')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Defining a few useful functions\n",
    "\n",
    "- `cdf_name()`: to construct the CDF output file name with a template \n",
    "- `time_ymdhm()`: to format time stamps into YYYYDDMMHHMM (for the file name)\n",
    "- `mission_phase()`: to get the mission phase from a time stamp."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def cdf_name(start_time: Time, version: str) -> str:\n",
    "    \"\"\"\n",
    "    get the CDF file name, built from a template, with start time and version. \n",
    "    :param start_ymdhm: Start time\n",
    "    :param version: CDF collection version tag\n",
    "    \"\"\"\n",
    "    return f'co_rpws_hfr_qtn_{time_ymdhm(start_time)}_v{version}.cdf'\n",
    "\n",
    "\n",
    "def time_ymdhm(epoch: Time) -> str:\n",
    "    \"\"\"\n",
    "    Get epoch time as a YYYYMMDDHHMM formated string \n",
    "    :param epoch: Epoch time\n",
    "    \"\"\"\n",
    "    return epoch.to_datetime().strftime(\"%Y%m%d%H%M\")\n",
    "\n",
    "def mission_phase(start_time):\n",
    "    mission_phase = 'Tour>Prime Mission'\n",
    "    if start_time >= Time(\"2008-07-01\"):\n",
    "        mission_phase = 'Extended Mission>Equinox Mission'\n",
    "    if start_time >= Time(\"2010-10-12\"):\n",
    "        mission_phase = 'Extended-Extended Mission>Solstice Mission'\n",
    "    if start_time >= Time(\"2016-11-01\"):\n",
    "        mission_phase = 'Extended-Extended Mission>Proximal Orbits'\n",
    "    return mission_phase\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Loading data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "idlsav_file = data_path / 'idl' / 'neTecore_rev00A_Vimp_lt_20pc__no_outliers_PDS.sav'\n",
    "imported_data =  readsav(idlsav_file)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "imported_data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Storing a few variables for the demo\n",
    "\n",
    "We deal here with a time series of plasma density and temperature. \n",
    "\n",
    "- `times`: the time stamps of the measurements\n",
    "- `ne_cc`: the electron density (in cc)\n",
    "- `te_ev`: the election temperature (in eV)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "times = Time(imported_data['tempsjd_nsweep_v2_orbit'], format='jd')\n",
    "ne_cc = imported_data['ne_core_cc_cleaned_orbit']\n",
    "te_ev = imported_data['te_core_ev_cleaned_orbit']\n",
    "\n",
    "start_time, end_time = times[0], times[-1]\n",
    "print(f'Data ranging from {start_time.isot} to {end_time.isot}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will need to provide time sampling step statistics, so we compute the time interval between successive samples "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "delta_times = (times[1:] - times[:-1]).to('second').value"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Preparing the CDF file\n",
    "\n",
    "Fist we define the CDF file name "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cdf_file = data_path / 'data' / cdf_name(start_time, '00')\n",
    "cdf_file.parent.mkdir(exist_ok=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cdf_file"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**NB**: if the CDF file already exists, its creation will lead to an error, so we first remove it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "if cdf_file.exists():\n",
    "    cdf_file.unlink()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## CDF file with space.pycdf\n",
    "\n",
    "We don't use CDF Skeleton here, all is defined in the code, in a nice pythonic way.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pycdf.lib.set_backward(False)  # this is setting the CDF version to be used\n",
    "\n",
    "cdf = pycdf.CDF(str(cdf_file), '')\n",
    "cdf.col_major(True)  # Column Major\n",
    "cdf.compress(pycdf.const.NO_COMPRESSION)  # No file level compression\n",
    "\n",
    "cdf"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The CDF object is now ready to receive attributes and variables.\n",
    "\n",
    "### We start with ISTP global attributes"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Writing ISTP global attributes\n",
    "\n",
    "cdf.attrs[\"Project\"] = [\"PADC>Paris Astronomical Data Centre\",\n",
    "                        \"CDPP>Plasma Physics Data Centre\",\n",
    "                        \"PDS-PPI>Planetary Plasma Interaction Node of NASA Planetary Data System\"]\n",
    "cdf.attrs['Discipline'] = \"Planetary Physics>Particles\"\n",
    "cdf.attrs['Data_type'] = 'HFR_QTN'\n",
    "cdf.attrs['Descriptor'] = 'RPWS'\n",
    "cdf.attrs['Data_version'] = \"00\"\n",
    "cdf.attrs['Instrument_type'] = 'Radio and Plasma Waves (space)'\n",
    "cdf.attrs['Logical_file_id'] = cdf_file.stem\n",
    "cdf.attrs['Logical_source'] = 'co_rpws_hfr_qtn'\n",
    "cdf.attrs['Logical_source_description'] = 'Quasi Thermal Noise Spectroscopy on High Frequency Receiver Data'\n",
    "cdf.attrs['File_naming_convention'] = 'source_descriptor_type_yyyyMMddHHmm_ver'\n",
    "cdf.attrs['Mission_group'] = 'Cassini-Huygens'\n",
    "cdf.attrs['PI_name'] = \"D.A. Gurnett\"\n",
    "cdf.attrs['PI_affiliation'] = 'University of Iowa'\n",
    "cdf.attrs['Source_name'] = 'CO>Cassini Orbiter'\n",
    "cdf.attrs['TEXT'] = 'Quasi Thermal Noise Spectroscopy derived from Cassini RPWS HFR Data (Kurth et al., CO-V/E/J/S/SS-RPWS-3-RDR-LRFULL-V1.0, NASA PDS, 2004)'\n",
    "cdf.attrs['Generated_by'] = ['PADC/LESIA', 'CDPP']\n",
    "cdf.attrs['Generation_date'] = datetime.now().isoformat()\n",
    "cdf.attrs['LINK_TEXT'] = [\"More details on \", \"CDPP archive\"]\n",
    "cdf.attrs['LINK_TITLE'] = [\"LESIA Cassini Kronos webpage\", \"web site\"]\n",
    "cdf.attrs['HTTP_LINK'] = [\"http://www.lesia.obspm.fr/kronos\", \"https://cdpp-archive.cnes.fr\"]\n",
    "cdf.attrs['MODS'] = \" \"\n",
    "cdf.attrs['Parents'] = str(idlsav_file)\n",
    "cdf.attrs['Rules_of_use'] = \" \"\n",
    "cdf.attrs['Time_resolution'] = f\"{numpy.mean(delta_times)} Seconds\"\n",
    "cdf.attrs['Acknowledgement'] = \" \"\n",
    "cdf.attrs['ADID_ref'] = \" \"\n",
    "cdf.attrs['Validate'] = \" \"\n",
    "\n",
    "cdf.attrs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Then we put PDS related global attributes"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Writing PDS/PPI global attributes\n",
    "\n",
    "cdf.attrs['PDS_orbit_number'] = idlsav_file.stem[12:15]\n",
    "cdf.attrs['PDS_mission_phase'] = mission_phase(start_time)\n",
    "cdf.attrs['PDS_start_time'] = start_time.to_datetime().isoformat()[:-3]+'Z'\n",
    "cdf.attrs['PDS_stop_time'] = end_time.to_datetime().isoformat()[:-3]+'Z'\n",
    "cdf.attrs['PDS_observation_target'] = \"Saturn\"\n",
    "cdf.attrs['PDS_observation_type'] = \"Particles\"\n",
    "cdf.attrs['PDS_collection_id'] = \"urn:nasa:pds:co-rpws-saturn:hfr-qtn-data\"\n",
    "cdf.attrs['PDS_LID'] = f\"urn:nasa:pds:co-rpws-saturn:hfr-qtn-data:{time_ymdhm(start_time)}-{time_ymdhm(end_time)}-cdf\"\n",
    "cdf.attrs['PDS_LID_plot'] = f\"urn:nasa:pds:co-rpws-saturn:hfr-qtn-browse:{time_ymdhm(start_time)}-{time_ymdhm(end_time)}-plot\"\n",
    "cdf.attrs['PDS_LID_thumbnail'] = f\"urn:nasa:pds:co-rpws-saturn:hfr-qtn-browse:{time_ymdhm(start_time)}-{time_ymdhm(end_time)}-thumbnail\"\n",
    "cdf.attrs['PDS_LID_index'] = \"urn:nasa:pds:co-rpws-saturn:hfr-qtn-document:index\"\n",
    "cdf.attrs['PDS_RDR_data_set_id'] = \"CO-V/E/J/S/SS-RPWS-3-RDR-LRFULL-V1.0\"\n",
    "\n",
    "cdf.attrs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### And finally a few others (VESPA, SPASE...)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Writing extra global attributes\n",
    "\n",
    "cdf.attrs['file_plot_name'] = f\"{cdf_file.stem}.pdf\"\n",
    "cdf.attrs['file_thumbnail_name'] = f\"{cdf_file.stem}.png\"\n",
    "\n",
    "# Writing VESPA global attributes\n",
    "\n",
    "cdf.attrs['VESPA_dataproduct_type'] = \"TS>Time Series\"\n",
    "cdf.attrs['VESPA_target_class'] = 'planet'\n",
    "# cdf.attrs['VESPA_target_name'] = '@PDS_observation_target'\n",
    "\n",
    "cdf.attrs['VESPA_time_sampling_step_value'] = f'{numpy.mean(delta_times)}'\n",
    "\n",
    "cdf.attrs['VESPA_instrument_name'] = \"RPWS>Radio and Plasma Waves Science\"\n",
    "cdf.attrs['VESPA_receiver_name'] = \"HFR>High Frequency Receiver\"\n",
    "cdf.attrs['VESPA_measurement_type'] = [\"phys.density;phys.electron\", \"phys.temperature;phys.electron\"]\n",
    "\n",
    "# Writing SPASE Global Attributes\n",
    "\n",
    "cdf.attrs['spase_DatasetResourceID'] = 'spase://INSU/NumericalData/Cassini/RPWS/QTN/PT32S'\n",
    "\n",
    "cdf.attrs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The time variable `EPOCH`\n",
    "\n",
    "ISTP recommendation requires `CDF_TIME_TT2000` Epoch variable type"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# SETTING UP VARIABLES AND VARIABLE ATTRIBUTES\n",
    "#   The EPOCH variable type must be CDF_TIME_TT2000\n",
    "#   PDS-CDF requires no compression for variables.\n",
    "\n",
    "cdf.new('EPOCH', data=times.to_datetime(), type=pycdf.const.CDF_TIME_TT2000, compress=pycdf.const.NO_COMPRESSION)\n",
    "cdf['EPOCH'].attrs.new('VALIDMIN', data=datetime(2004, 6, 1), type=pycdf.const.CDF_TIME_TT2000)\n",
    "cdf['EPOCH'].attrs.new('VALIDMAX', data=datetime(2017, 9, 30), type=pycdf.const.CDF_TIME_TT2000)\n",
    "cdf['EPOCH'].attrs.new('SCALEMIN', data=start_time.to_datetime().replace(\n",
    "    microsecond=0, second=0, minute=0, hour=0), type=pycdf.const.CDF_TIME_TT2000)\n",
    "cdf['EPOCH'].attrs.new('SCALEMAX', data=end_time.to_datetime().replace(\n",
    "    microsecond=0, second=0, minute=0, hour=0) + timedelta(days=1), type=pycdf.const.CDF_TIME_TT2000)\n",
    "cdf['EPOCH'].attrs['CATDESC'] = \"Default time (TT2000)\"\n",
    "cdf['EPOCH'].attrs['FIELDNAM'] = \"Epoch\"\n",
    "cdf['EPOCH'].attrs.new('FILLVAL', data=-9223372036854775808, type=pycdf.const.CDF_TIME_TT2000)\n",
    "cdf['EPOCH'].attrs['LABLAXIS'] = \"Epoch\"\n",
    "cdf['EPOCH'].attrs['UNITS'] = \"ns\"\n",
    "cdf['EPOCH'].attrs['FORM_PTR'] = \"CDF_TIME_TT2000\"\n",
    "cdf['EPOCH'].attrs['VAR_TYPE'] = \"support_data\"\n",
    "cdf['EPOCH'].attrs['SCALETYP'] = \"linear\"\n",
    "cdf['EPOCH'].attrs['MONOTON'] = \"INCREASE\"\n",
    "cdf['EPOCH'].attrs['REFERENCE_POSITION'] = \"Spacecraft barycenter\"\n",
    "cdf['EPOCH'].attrs['SI_CONVERSION'] = \"1.0e-9>s\"\n",
    "cdf['EPOCH'].attrs['UCD'] = \"time.epoch\"\n",
    "cdf['EPOCH'].attrs['TIME_BASE'] = 'UTC'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cdf['EPOCH'].attrs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Writing the two other variable"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Writing NE_CORE variable\n",
    "\n",
    "cdf.new('NE_CORE', data=ne_cc, type=pycdf.const.CDF_REAL4,\n",
    "    compress=pycdf.const.NO_COMPRESSION)\n",
    "cdf['NE_CORE'].attrs['CATDESC'] = \"Core Electron Density\"\n",
    "cdf['NE_CORE'].attrs['DEPEND_0'] = \"EPOCH\"\n",
    "cdf['NE_CORE'].attrs['DICT_KEY'] = \"density>number\"\n",
    "cdf['NE_CORE'].attrs['DISPLAY_TYPE'] = \"time_series\"\n",
    "cdf['NE_CORE'].attrs['FIELDNAM'] = 'NE_CORE'\n",
    "cdf['NE_CORE'].attrs.new('FILLVAL', data=-1.0e+31, type=pycdf.const.CDF_REAL4)\n",
    "cdf['NE_CORE'].attrs['FORMAT'] = \"E12.2\"\n",
    "cdf['NE_CORE'].attrs['LABLAXIS'] = 'Core Electron Density'\n",
    "cdf['NE_CORE'].attrs['UNITS'] = \"cm**-3\"\n",
    "cdf['NE_CORE'].attrs.new('VALIDMIN', data=0, type=pycdf.const.CDF_REAL4)\n",
    "cdf['NE_CORE'].attrs.new('VALIDMAX', data=1e6, type=pycdf.const.CDF_REAL4)\n",
    "cdf['NE_CORE'].attrs['VAR_TYPE'] = \"data\"\n",
    "cdf['NE_CORE'].attrs['SCALETYP'] = \"log\"\n",
    "cdf['NE_CORE'].attrs.new('SCALEMIN', data=0.1, type=pycdf.const.CDF_REAL4)\n",
    "cdf['NE_CORE'].attrs.new('SCALEMAX', data=200, type=pycdf.const.CDF_REAL4)\n",
    "cdf['NE_CORE'].attrs['SI_CONVERSION'] = \"1.0e-6>m**-3\"\n",
    "cdf['NE_CORE'].attrs['UCD'] = \"phys.density;phys.electron\"\n",
    "\n",
    "# Writing TE_CORE variable\n",
    "\n",
    "cdf.new('TE_CORE', data=te_ev, type=pycdf.const.CDF_REAL4,\n",
    "    compress=pycdf.const.NO_COMPRESSION)\n",
    "cdf['TE_CORE'].attrs['CATDESC'] = \"Core Electron Temperature\"\n",
    "cdf['TE_CORE'].attrs['DEPEND_0'] = \"EPOCH\"\n",
    "cdf['TE_CORE'].attrs['DICT_KEY'] = \"temperature\"\n",
    "cdf['TE_CORE'].attrs['DISPLAY_TYPE'] = \"time_series\"\n",
    "cdf['TE_CORE'].attrs['FIELDNAM'] = 'TE_CORE'\n",
    "cdf['TE_CORE'].attrs.new('FILLVAL', data=-1.0e+31, type=pycdf.const.CDF_REAL4)\n",
    "cdf['TE_CORE'].attrs['FORMAT'] = \"E12.2\"\n",
    "cdf['TE_CORE'].attrs['LABLAXIS'] = 'Core Electron Temperature'\n",
    "cdf['TE_CORE'].attrs['UNITS'] = \"eV\"\n",
    "cdf['TE_CORE'].attrs.new('VALIDMIN', data=0, type=pycdf.const.CDF_REAL4)\n",
    "cdf['TE_CORE'].attrs.new('VALIDMAX', data=1e6, type=pycdf.const.CDF_REAL4)\n",
    "cdf['TE_CORE'].attrs['VAR_TYPE'] = \"data\"\n",
    "cdf['TE_CORE'].attrs['SCALETYP'] = \"linear\"\n",
    "cdf['TE_CORE'].attrs.new('SCALEMIN', data=0, type=pycdf.const.CDF_REAL4)\n",
    "cdf['TE_CORE'].attrs.new('SCALEMAX', data=20, type=pycdf.const.CDF_REAL4)\n",
    "cdf['TE_CORE'].attrs['UCD'] = \"phys.temperature;phys.electron\"\n",
    "cdf['TE_CORE'].attrs['SI_CONVERSION'] = \"11604.5250061657>K\"\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cdf"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cdf.close()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Validating the CDF\n",
    "\n",
    "Several tools are available to validate a CDF file for PDS4 compliance. \n",
    "\n",
    "- **pds-cdf/cdfcheck**: This script is provided by NASA PDS/PPI node (http://release.igpp.ucla.edu/pds/cdf/).\n",
    "- **skteditor/spdfjavaClasses**: This script proposes checks related to ISTP validation. Latest version is here: https://spdf.gsfc.nasa.gov/skteditor/standalone-skteditor-1.3.3.zip"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "skteditor = 'lib/skteditor/spdfjavaClasses.jar'\n",
    "cdfcheck = 'lib/pds-cdf/bin/cdfcheck'\n",
    "cdfconvert = '/Applications/cdf/cdf/bin/cdfconvert'\n",
    "cdfjava = '/Applications/cdf/cdf/cdfjava/classes/cdfjava.jar'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Check CDF functions\n",
    "\n",
    "Runs `cdfcheck` from *PDS-CDF* and `CDFCheck` from *SKTEditor*."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import subprocess\n",
    "\n",
    "def check_cdf(cdf_file, verbose=False):\n",
    "\n",
    "    log = ''\n",
    "    \n",
    "    verb_flag = \"\"\n",
    "    if verbose:\n",
    "        verb_flag = '-v'\n",
    "        \n",
    "    # Running PDS CDFCHECK\n",
    "    command = [cdfcheck, verb_flag, cdf_file]\n",
    "    print(f\"Running:\\n{' '.join(command)}\\n\")\n",
    "    p = subprocess.Popen(command, stdout=subprocess.PIPE)\n",
    "    log += p.communicate()[0].decode('ascii')\n",
    "    \n",
    "    # Running SKTEditor\n",
    "    command = ['java', '-cp', f'{skteditor}:{cdfjava}', 'gsfc.spdf.istp.tools.CDFCheck', cdf_file]\n",
    "    print(f\"Running:\\n{' '.join(command)}\\n\")\n",
    "    p = subprocess.Popen(command, stdout=subprocess.PIPE)\n",
    "    log += p.communicate()[0].decode('ascii')\n",
    "    \n",
    "    return log"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "check_log = check_cdf(str(cdf_file))\n",
    "print('Result:')\n",
    "print(check_log)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*It is a good practice to update the CDF `Validate` global attribute with the validation result.*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Let's create the CDF XML Label\n",
    "\n",
    "For this, we use a set of scripts developed by NASA PDS/PPI, called `docgen`. It is based on a *velocity* template system, which is well adapted for XML rendering. The latest version of this library is available here: http://release.igpp.ucla.edu/igpp/docgen/"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Defining a few useful functions\n",
    "\n",
    "- `_md5_hash()`: computes the MD5 hash of a file \n",
    "- `_get_now_tmp_file()`: builds a temporay file, with the current time stamp\n",
    "- `_get_info_tmp_file()`: builds a temporary file, with metadata (name, size, date, hash) from a file\n",
    "\n",
    "The two temporary files are used to build the PDS4 XML label"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def _md5_hash(fname):\n",
    "    # from: https://stackoverflow.com/questions/3431825/generating-an-md5-checksum-of-a-file\n",
    "    import hashlib\n",
    "    hash_md5 = hashlib.md5()\n",
    "    with open(fname, \"rb\") as f:\n",
    "        for chunk in iter(lambda: f.read(4096), b''):\n",
    "            hash_md5.update(chunk)\n",
    "    return hash_md5.hexdigest()\n",
    "\n",
    "def _get_now_tmp_file():\n",
    "    tmp_file = Path('/tmp/tmp_now.lst')\n",
    "    with open(str(tmp_file), 'w') as f:\n",
    "        f.write('value = {}\\n'.format(datetime.now().date().isoformat()))\n",
    "    return tmp_file\n",
    "\n",
    "def _get_info_tmp_file(cur_file):\n",
    "    info_file = Path('/tmp/info.lst')\n",
    "    with open(str(info_file), 'w') as f:\n",
    "        f.write('name = {}\\n'.format(str(cur_file.name)))\n",
    "        f.write('size = {}\\n'.format(os.path.getsize(str(cur_file))))\n",
    "        f.write('date = {}\\n'.format(datetime.fromtimestamp(os.path.getmtime(str(cur_file))).\n",
    "                                     strftime('%Y-%m-%dT%H:%M:%S')))\n",
    "        f.write('hash = {}\\n'.format(_md5_hash(str(cur_file))))\n",
    "    return info_file\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We define the path to the `docgen` script, first.\n",
    "\n",
    "Then define the template to be used, the name of the PDS4 XML label file, and computes temporary files."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "docgen_bin = 'lib/igpp-docgen/bin/docgen'\n",
    "vm_file = 'templates/co_rpws_hfr_qtn_v00_data_cdf.vm'\n",
    "xml_label = cdf_file.parent / f\"{cdf_file.stem}.xml\"\n",
    "\n",
    "\n",
    "info_file = _get_info_tmp_file(cdf_file)\n",
    "now_file = _get_now_tmp_file()\n",
    "\n",
    "!cat /tmp/info.lst"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's have a look to the template file"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!open -a \"Oxygen XML Editor\" templates/co_rpws_hfr_qtn_v00_data_cdf.vm"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Prepare the script command line and run it"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "command = [docgen_bin, f\"cdf:{cdf_file}\", f'now:{now_file}', f'info:{info_file}', \n",
    "    f'{vm_file}', '>', f\"{xml_label}\"]\n",
    "\n",
    "print(' '.join(command))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "os.system(' '.join(command))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's have a look to the CDF file"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!open demo_data/data/co_rpws_hfr_qtn_200410271900_v00.xml"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Same process for Browse products labels\n",
    "\n",
    "We use another template file, but we process the same CDF file, since in contains all the required metadata"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "vm_file = 'templates/co_rpws_hfr_qtn_v00_browse_plot.vm'\n",
    "\n",
    "with pycdf.CDF(str(cdf_file)) as cdf:\n",
    "    pdf_file_name = str(cdf.attrs['file_plot_name'])\n",
    "\n",
    "pdf_file = data_path / 'browse' / pdf_file_name\n",
    "info_file = _get_info_tmp_file(pdf_file)\n",
    "now_file = _get_now_tmp_file()\n",
    "\n",
    "print(f'{pdf_file}\\n')\n",
    "\n",
    "!cat /tmp/info.lst"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "xml_label = pdf_file.parent / f\"{pdf_file.stem}.xml\"\n",
    "\n",
    "command = [docgen_bin, f\"cdf:{cdf_file}\", f'now:{now_file}', f'info:{info_file}', \n",
    "    f'{vm_file}', '>', f\"{xml_label}\"]\n",
    "\n",
    "print(' '.join(command))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "os.system(' '.join(command))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!open demo_data/browse/co_rpws_hfr_qtn_200410271900_v00.pdf"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!open demo_data/browse/co_rpws_hfr_qtn_200410271900_v00.xml"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# More detailed example\n",
    "\n",
    "https://gitlab.obspm.fr/cecconi/cassini-rpws-hfr-qtn-archive-pds4\n",
    "\n",
    "This repository details also the inventory and bundle generation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
